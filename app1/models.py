from django.db import models
from .const import STOPWORDS
import re
import requests
from bs4 import BeautifulSoup

class Token(models.Model):
    value = models.TextField()

class Question(models.Model):
    sidid = models.IntegerField(default=0)
    title = models.TextField(null=False)
    content = models.TextField(null=True, blank=True)
    tag_val = models.CharField(null=True, max_length=100,blank=True)
    answer = models.TextField(null=True, blank=True)
    url = models.URLField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.title


    def set_tags(self):
        title = self.title.lower().split()
        keywords = [word for word in title if not word in STOPWORDS]
        keywords = [word for word in keywords if len(word)>2]
        self.tag_val = " ".join(keywords)
        #print(title)
        #self.save()

    @property
    def get_next_id(self):
        try:
            next = Question.objects.filter(id__gt=self.id, url=None)[0].id
        except:
            next = Question.objects.filter(url=None).first().id
        return next

    def preProcessAnswer(self):
        # b = a.replace("/\&amp\;/gm","&")
        #a = "<img src='https://www.ques10.com/static/biostar2.logo.png?v=14'>"
        a = self.answer
        a = removeAmp(a)
        a = removeAnswerWord(a)
        soup = BeautifulSoup(a,'html.parser')
        img_tags = soup.find_all("img")
        for i in img_tags:
            if "https://" in i['src'] or "http://" in i['src']:
                imgData = i['src']
            else:
                imgData = i['src'].split("base64,")[1]
            #print(type(imgData))
            imgurData = uploadImage(imgData)
            #print(imgurData)
            if imgurData['status'] == 200:
                imgur_link = imgurData['data']['link']
                a = a.replace(i['src'], imgur_link)
                #print(a)
        soup = BeautifulSoup(a,'html.parser')
        spans = soup.find_all('span', {'class' : 'math-tex'})
        for s in spans:
            math = s.text
            f1 = math.replace('\(','$')
            f2 = f1.replace('\)','$')
            f3 = f2.replace('$$','$')
            # f4 = f3.replace('\\\\','\\\\\\\\')
            # final = re.sub('/\$\$/gm',"$", re.sub('/\\\\/gm','\\\\\\\\', math))
            a = a.replace(math, f3)
        # print(BeautifulSoup(a).prettify())
        soup = BeautifulSoup(a,'html.parser')
        tbodies = soup.find_all('tbody') 
        for tbody in tbodies:
            first_row_cells = tbody.tr.find_all('td') 
            if len(first_row_cells) != 0:
                for cell in first_row_cells:
                    cell.name='th'
        a = str(soup)
        self.answer = a
        #self.save()

def uploadImage(imgData):
    url = 'https://api.imgur.com/3/image'
    headers = { 'Authorization': 'Client-ID b3e7bac9f8c0426', 'X-Mashape-Key': '35fe3e8e8fmsh1d6e2b904131c2ep1723d3jsnef19b48425b1'}
    response = requests.post(url, headers=headers, json={'image' : imgData})
    # print(response.text)
    return response.json()

def removeAmp(txt):
    return txt.replace('&amp;','&')

def replaceNonAscii(txt):
    txt = ascii(txt)
    non_ascii_u = re.findall(r'\\u[A-Za-z0-9]{4}', txt)
    non_ascii_x = re.findall(r'\\x[A-Za-z0-9]{2}', txt)
    non_ascii_U = re.findall(r'\\U[A-Za-z0-9]{8}', txt)
    non_ascii = non_ascii_u +  non_ascii_x + non_ascii_U
    for i in non_ascii:
        code = i[2:]
        ascii_eqv = r"$\unicode{x"+code+"}$"
        txt = txt.replace(i,ascii_eqv)
    return txt

def removeAnswerWord(txt):
    return txt.replace("<p><em><strong>Answer:</strong></em></p>", "").replace("<p><strong><em>Answer:</em></strong></p>", "").replace("<p><em><strong>Answer: </strong></em></p>", "").replace("<p><strong><em>Answer: </em></strong></p>", "").replace("<p><em><strong>Answer:  </strong></em></p>", "").replace("<p><em><strong>Answer :</strong></em></p>", "").replace("<p><em><strong>Answer : </strong></em></p>", "")
