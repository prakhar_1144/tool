from django.urls import path
from .views import call_api, PostListView, PostUpdateView
urlpatterns = [
    path('', PostListView.as_view()),
    path('<pk>/', PostListView.as_view(), name="home"),
    path('call_api/<pk>/', call_api, name="call_api"),
    path('post_update/<pk>/', PostUpdateView.as_view(), name="post_update")
]