from django.shortcuts import redirect, reverse
from django.http import Http404
from django.utils.translation import gettext_lazy as _
import requests
from django.views.generic.list import ListView
from django.views.generic.edit import UpdateView
from django.views.generic.detail import DetailView
from .models import Question, Token
from markdownify import markdownify as md
from .models import removeAmp, replaceNonAscii
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin


class PostListView(LoginRequiredMixin, DetailView):
    model = Question

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.url is not None:
            next_id = self.object.get_next_id
            return redirect('home', pk=next_id)
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        pk = self.kwargs.get(self.pk_url_kwarg, Question.objects.filter(url=None).first().id)
        if pk is not None:
            queryset = queryset.filter(pk=pk)
        try:
            obj = queryset.get()
            if obj.tag_val is None:
                obj.set_tags()
                obj.preProcessAnswer()
                obj.title = removeAmp(obj.title)
                obj.save()
        except queryset.model.DoesNotExist:
            raise Http404(_("No %(verbose_name)s found matching the query") %
                          {'verbose_name': queryset.model._meta.verbose_name})
        return obj

class PostUpdateView(UpdateView):
    model = Question
    template_name_suffix = '_update_form'
    fields = ['title','content','tag_val']
    def get_success_url(self):
        return reverse('home', args=[self.object.id])

def call_api(request, pk):
    pk = int(pk)
    post = Question.objects.get(id=pk)
    answer_markdown = correctSubscript(md(post.answer))
    title_markdown = correctSubscript(md(post.title))
    tag_val_markdown = correctSubscript(md(post.tag_val))
    #print(answer_markdown)
    try:
        token = Token.objects.first().value
    except:
        messages.error(request, 'No token available !')
        return redirect('home', pk=pk)

    headers = {"Authorization":"JWT " + token}

    if not post.content:
        post.content = ""
    
    question = {"title" : title_markdown, "content" : post.content, "tag_val" : tag_val_markdown, "type":0 }
    

    qresponse = requests.post( settings.TARGET_IP +"api/v1/q/new/", json=question, headers=headers)
    #print(qresponse.json())
    # if status code == 500 then theta wala not posted
    if qresponse.status_code == 401:
        messages.error(request, 'Please update your token to continue.')
        return redirect('home', pk=pk)
    
    if qresponse.status_code == 201: # if answer exists
        post.url = settings.TARGET_IP + 'p/' + str(qresponse.json()['pk'])
        post.save()
        answer = {"pid" : qresponse.json()["pk"], "content" : answer_markdown, "type":1}
        aresponse = requests.post( settings.TARGET_IP +"api/v1/a/new/", json=answer, headers=headers)
        #print("a",aresponse.json())

    next_id = post.get_next_id
    return redirect('home', pk=next_id)

def correctSubscript(txt):
    return txt.replace('\_','_')
