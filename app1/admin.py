from django.contrib import admin
from .models import Question, Token
# Register your models here.

class QuestionAdmin(admin.ModelAdmin):
    list_display = ['title', 'url']
    search_fields = ['title']

admin.site.register(Question, QuestionAdmin)
admin.site.register([Token])
