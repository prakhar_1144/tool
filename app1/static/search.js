function initializeSuggestPosts() {
    var title = $(".title").text();
    //console.log(title);
    var accordion = `
        <br>
        <div class="accordion" id="accordionExample">
            <div class="accordion-item">
            <h2 class="accordion-header" id="headingOne">
                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                Similar Questions
                </button>
            </h2>
            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                
                </div>
            </div>
            </div>
        </div>
        `
    $.ajax({
	url: 'https://www.ques10.com/local/search/p/suggestions/',
        dataType: 'json',
        data: {q:title},
        success: function (data) {
            //console.log(data)
            if (data.length > 0){
                if ($("#accordion").length == 0){
                    $(accordion).insertAfter($(".title"));
                }
                var ques =
                	'<ul style="padding-left:10px;">' +
                		data.map(function (data) {
                		    var id = data.split(' ')[0];
                			return '<li><a target="_blank" style="text-decoration:none;" href="https://ques10.com/p/'+ id +'">' + data + '</a></li>';
                		}).join('') +
                	'</ul>';
                $(".accordion-body").html(ques);
                
            }
            else {
                $("#accordion").remove();
            }
        }
    })
}
// initializeSuggestPosts();
$(document).ready(function () {
    initializeSuggestPosts();
    console.log("2");
})
